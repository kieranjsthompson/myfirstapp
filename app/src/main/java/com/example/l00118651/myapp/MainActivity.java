package com.example.l00118651.myapp;


import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

// this is my first comment
public class MainActivity extends ActionBarActivity {

    MediaPlayer mediaPlayer;

    private TextView myTextView;
    private Button myFirstButton;
    private Button mySecondButton;
    private Button myStopButton;
    private Button myThirdButton;
    private Button myFourthButton;
    private Button myFifthButton;
    private Button mySixthButton;
    private Button mySeventhButton;
    private Button myEighthButton;
    private Button myNinethButton;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mediaPlayer = MediaPlayer.create(this, R.raw.dog);//raw/s.mp3
        //mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.start();
        mediaPlayer.stop();
        mediaPlayer.release();

        myTextView = (TextView) findViewById(R.id.myTextview);
        myFirstButton = (Button) findViewById(R.id.myFirstButton);
        mySecondButton = (Button) findViewById(R.id.mySecondButton);
        myStopButton = (Button) findViewById(R.id.myStopButton);
        myThirdButton = (Button) findViewById(R.id.myThirdButton);
        myFourthButton = (Button) findViewById(R.id.myFourthButton);
        myFifthButton = (Button) findViewById(R.id.myFifthButton);
        mySixthButton = (Button) findViewById(R.id.mySixthButton);
        mySeventhButton = (Button) findViewById(R.id.mySeventhButton);
        myEighthButton = (Button) findViewById(R.id.myEighthButton);
        myNinethButton = (Button) findViewById(R.id.myNinethButton);


        myFirstButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Toast.makeText(getApplicationContext(), "Why did the chicken cross the road?", Toast.LENGTH_LONG).show();
                //Toast.makeText(getApplicationContext(), "Because he's a bollocks", Toast.LENGTH_LONG).show();
                myTextView.setText("MyFunnyApp");


                    mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.dog);//raw/s.mp3

                    mediaPlayer.start();



                //mediaPlayer = MediaPlayer.create(this, R.raw.dog);//raw/s.mp3
                //mediaPlayer.setOnCompletionListener(this);
                //mediaPlayer.start();
            }
        });

        mySecondButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer = mediaPlayer.create(getApplicationContext(), R.raw.ohbaby);

                mediaPlayer.start();
            }
        });

        myThirdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer = mediaPlayer.create(getApplicationContext(), R.raw.spooky);

                mediaPlayer.start();
            }
        });

        myFourthButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer = mediaPlayer.create(getApplicationContext(), R.raw.smokeweedeveryday);

                mediaPlayer.start();
            }
        });

        myFifthButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer = mediaPlayer.create(getApplicationContext(), R.raw.dankstorm);

                mediaPlayer.start();
            }
        });

        mySixthButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer = mediaPlayer.create(getApplicationContext(), R.raw.wow);

                mediaPlayer.start();
            }
        });

        mySeventhButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer = mediaPlayer.create(getApplicationContext(), R.raw.hitmarker);

                mediaPlayer.start();
            }
        });

        myEighthButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer = mediaPlayer.create(getApplicationContext(), R.raw.sanic);

                mediaPlayer.start();
            }
        });
        myNinethButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer = mediaPlayer.create(getApplicationContext(), R.raw.airhorn);

                mediaPlayer.start();
            }
        });

        myStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mediaPlayer.stop();
            }
        });
    }

    ;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
